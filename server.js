
const express         = require('express');
const https           = require('https');
const fs              = require('fs');
const bodyParser      = require('body-parser');
const context         = require('aws-lambda-mock-context');

// lambda.js contains the lambda function for Alexa as in https://github.com/alexa/alexa-skills-kit-sdk-for-nodejs
var   lambda          = require('./lambda');

const SERVER_PORT     = 443;
const SERVER_IP       = "traoumad.evl.uic.edu";

// SSL Certificate stuff for https 
var privateKey  = fs.readFileSync("keys/_.evl.uic.edu.key", 'utf8');
var certificate = fs.readFileSync("keys/_.evl.uic.edu.crt", 'utf8');
var ca = fs.readFileSync("keys/_.evl.uic.edu-ca.crt").toString();
var credentials = {key: privateKey, cert: certificate,ca:ca};

const app = express();

app.use(bodyParser.json({ type: 'application/json' }));

// your service will be available on <YOUR_IP>/alexa
app.post('/alexa/', function (req, res) {
    console.log('Got a post');
    var ctx = context();
    lambda.handler(req.body,ctx);
    ctx.Promise
        .then(resp => {  return res.status(200).json(resp); })
        .catch(err => {  console.log(err); })
});

var httpsServer = https.createServer(credentials, app);

httpsServer.listen(SERVER_PORT, SERVER_IP, function () {
    console.log('Alexa Skill service ready on ' + SERVER_IP+":"+SERVER_PORT+" via https. Be happy!");
});


